import java.util.Scanner;

public class Diapozon {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите максимальный диапазон, не включая данное число");
        int c = scanner.nextInt();
        int k = 0;
        while (c != 18) {
            c--;
            if (c % 2 == 0) {
                k++;
            }
        }
        System.out.println(k);
    }
}



