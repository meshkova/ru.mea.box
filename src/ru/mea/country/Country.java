package ru.mea.country;


public class Country {


    public String name;
    public String capital;
    public double area;
    public int population;


    public Country(String name, String capital, double area, int population) {
        this.name = name;
        this.capital = capital;
        this.area = area;
        this.population = population;
    }


    public Country(Country input) {
        this("неизвестно", "неизвестно", 0, 0);
    }


    public String getName() {
        return name;
    }


    public double getArea() {
        return area;
    }

    public int getPopulation() {
        return population;
    }

    public double density() {
        return population / area;
    }


    @Override
    public String toString() {

        return
                "ColorBox{" + "Название" + name + "Столица" +
                        capital + "Площадь" +
                        area +
                        "Население" + population + '}';

    }


}

