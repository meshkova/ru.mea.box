package ru.mea.patient;

public class Patient {
    private String surname;
   private int yearOfBirth;
   private int cardNumber;
   private  boolean examinator;

    public Patient(String surname, int yearOfBirth, int cardNumber, boolean examinator) {
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
        this.cardNumber = cardNumber;
        this.examinator = examinator;
    }

    public Patient() {
        this("не указана", 0, 0, false);
    }

    public String getSurname() {
        return surname;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public boolean isExaminator() {
        return examinator;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "Фамилия " + surname + '\'' +
                ", Год рождения " + yearOfBirth +
                ", Номер карты " + cardNumber +
                ", Диспансеризация " + examinator +
                '}';
    }
}
