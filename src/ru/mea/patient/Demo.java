package ru.mea.patient;

import javax.swing.*;

public class Demo {
    public static void main(String[] args) {
        Patient dima = new Patient("Иванов", 2000, 5, true);
        Patient denis = new Patient("Раскин", 2000, 5, false);
        Patient vadim = new Patient("Папшев", 1989, 5, true);
        Patient lera = new Patient("Маленков", 1967, 5, true);
        Patient nikita = new Patient("Хакеев", 1999, 5, true);
        Patient[] people = {dima, denis, vadim, lera, nikita};
        System.out.println("Пациенты прошедшие диспансеризацию: ");
        Examinator(people);
    }

    /**
     * Сначала берёт из массива нулевое значение, сравнивает его через if с заданными данными и
     * если оно верно, то выводит этот элемент массива на экран.
     * Далее берет первое значение из массива и проделывает такие же действия.
     * @param people
     */
    public static void Examinator(Patient people[]) {


        for (int i = 0; i < people.length; i++) {
            boolean a1 = people[0 + i].isExaminator();
            int b1 = people[0 + i].getYearOfBirth();
            if (a1 == true && b1 < 2000) {
                System.out.println(people[i]);

            }

        }
    }
}