package ru.mea.bank;

public class Interest {
    private int money;
    private double inter;

    public Interest(int money, double inter) {
        this.money = money;
        this.inter = inter;
    }

    public int getMoney() {
        return money;
    }


    public double getInter() {
        return inter;
    }

    @Override
    public String toString() {
        return "Interest{" +
                "money=" + money +
                ", inter=" + inter + "%" +
                '}';
    }
    protected static void banks(Interest petrow){
        int k=100;
        double account=0;
        account = (petrow.getMoney()/k)*petrow.getInter();
        account = account + petrow.getMoney();
        System.out.println(account);
    }

}
