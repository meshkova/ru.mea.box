package ru.mea.shape;

public class Curcle extends  Shape {
    private Point center;
    private double radius;

    public Curcle(Color color, Point center, double radius) {
        super(color);
        this.center = center;
        this.radius = radius;
    }

    @Override

    public double area(){
        return Math.PI * radius * radius;

    }

    @Override
    public String toString() {
        return "Curcle{" +
                "цвет=" + getColor() +
                "center=" + center +
                ", radius=" + radius +
                '}';
    }
}
