package ru.mea.shape;

public class Square extends  Shape{
private double side;
private Point conter;




    public Square(Color color, double side, Point conter) {
        super(color);
        this.side = side;
        this.conter = conter;
    }
    public double area() {
        return side * side;
    }

    @Override
    public String toString() {
        return "Square{" +  "цвет=" +
                getColor() +
                "side=" + side +
                ", conter=" + conter +
                '}';
    }
}
