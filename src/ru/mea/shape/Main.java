package ru.mea.shape;

public class Main {
    public static void main(String[] args) {
        Curcle curcle = new Curcle(Color.BLUE, new Point(), 5);
        Riangle riangle = new Riangle(Color.BLACK, new Point(7, 13), new Point(5, 8), new Point(1, 2));
        Square square = new Square(Color.WHITE, 5, new Point());

        Shape[] shape = {curcle, riangle, square};
        double maxShape = MaxShapeArea(shape);

        System.out.println("Максимальная площадь:" + maxShape);
        Name(shape, curcle, riangle, square, MaxShapeArea(shape));
    }

    public static double MaxShapeArea(Shape[] shape) {
        double max = shape[0].area();
        for (int i = 0; i < shape.length; i++) {
            double area = shape[i].area();
            if (area > max) {
                max = area;
            }
        }
        return max;
    }


    public static void Name(Shape shape[], Curcle curcle, Riangle riangle, Square square, double max) {
        if (max == shape[0].area()) {
            System.out.println(curcle);
        } else if (max == shape[1].area()) {
            System.out.println(riangle);
        } else if (max == shape[2].area()) {
            System.out.println(square);
        }

    }

}
