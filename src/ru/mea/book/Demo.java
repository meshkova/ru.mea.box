package ru.mea.book;



public class Demo {
    public static void main(String[] args) {
        Book bools = new Book("Мушкин", "Зори", 2018);
        Book andrey = new Book("Петров", "Огонь", 2018);
        Book summer = new Book("Мальцева", "Земля", 2019);
        Book samara = new Book("Иванов", "Зима", 2018);
        Book solnzh = new Book("Фёдоров", "Мишка", 2018);
        Book[] books = {bools, andrey, summer, samara, solnzh};
/*Если год равняется  2018-му то компилятор выводит элемент
  После чего переходит  к другому элементу и тоже сравнивает

 Сравнивает первую книгу со второй с помощью вызванного метода

 */
        for (Book book : books) {
            if (book.category()) {
                System.out.println(book + " ");
            }
        }

        if (books[0].isSameCategory(books[1])) {
            System.out.println("У первой и второй книги, год издания одинаков");

        } else {
            System.out.println("Неодинаков");

        }
    }
}

