package ru.mea.book;

@SuppressWarnings("unused")
public class Book {
    private String surname;
    private String name;
    private int year;

    Book(String surname, String name, int year) {
        this.surname = surname;
        this.name = name;
        this.year = year;
    }

    public Book() {
        this("неизвестно", "неизвестно", 0);
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    private int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Book{" +
                "surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", year=" + year +
                '}';
    }

    boolean category() {

        return year == 2018;

    }

    /**
     * Если два года равны, то возвращает этот год
     * @return возвращает cat
     */
    private int cat() {
        int cat = 0;
        if (getYear() == getYear()) {
            cat = getYear();
            return cat;
        } else {
            return cat;
        }
    }

    /**Если год выпуска текущей книги равен году пришедшей в массив книги
     * то возвращает true
     * @param bools идентификатор
     * @return возвращает истину
     */
    boolean isSameCategory(Book bools) {
        return (this.getYear()) == (bools.cat());
    }
}
