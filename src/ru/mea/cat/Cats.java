package ru.mea.cat;

import java.util.ArrayList;

public class Cats extends ArrayList<Cats> {
    private String name;
    private Gender gender;
    private int year;

    public Cats(String name, Gender gender, int year) {
        this.name = name;
        this.gender = gender;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public Gender getGender() {
        return gender;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Cats{" +
                "name='" + name + '\'' +
                ", gender=" + gender +
                ", year=" + year +
                '}';
    }

}

