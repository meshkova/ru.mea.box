package ru.mea.robot;

public enum Direction {

    UP,
    RIGHT,
    DOWN,
    LEFT

}
