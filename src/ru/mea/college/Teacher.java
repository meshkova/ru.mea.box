package ru.mea.college;

public class Teacher extends Person {
    private String discipline;
private boolean curator;

    public Teacher(Gender gender, String surname, String discipline, boolean curator) {
        super(gender, surname);
        this.discipline = discipline;
        this.curator = curator;
    }

    public String getDiscipline() {
        return discipline;
    }

    public boolean isCurator() {
        return curator;
    }

    @Override
    public String toString() {
        return "Teacher{" + "пол=" +gender + ", фамилия=" + surname +
                ", discipline='" + discipline + '\'' +
                ", curator=" + curator +
                '}';
    }
}
