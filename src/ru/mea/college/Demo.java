package ru.mea.college;

import java.util.ArrayList;

public class Demo {
    public static void main(String[] args) {
        Student ivanova = new Student(Gender.FEMALE, "Иванова", 2017, "10.02.09");
        Student bylgakova = new Student(Gender.FEMALE, "Собакина", 2018, "10.02.09");
        Student lyshnikov = new Student(Gender.MALE, "Мишкин", 2017, "10.02.09");
        ArrayList<Student> students = new ArrayList<>();
        students.add(ivanova);
        students.add(bylgakova);
        students.add(lyshnikov);
        Teacher soleneva = new Teacher(Gender.FEMALE, "Соленева", "Математика", false);
        Teacher sol = new Teacher(Gender.FEMALE, "Сол", "Русский язык", false);
        Teacher ivanov = new Teacher(Gender.MALE, "Иванов", "Астрономия", true);

        ArrayList<Teacher> teachers = new ArrayList<>();
        teachers.add(soleneva);
        teachers.add(sol);
        teachers.add(ivanov);
        System.out.println("Девушки поступившие в 2017 году");
        years(students);
        System.out.println("Преподаватели - кураторы");
        curators(teachers);
        System.out.println("Мужчины");
        males(students, teachers);
    }

    public static void years(ArrayList<Student> students) {
        for (int i = 0; i < students.size(); i++) {

            if (2017 == students.get(i).getYear() && Gender.FEMALE == students.get(i).getGender()) {
                System.out.println(students.get(i));
            }

        }
    }

    public static void curators(ArrayList<Teacher> teachers) {
        for (int i = 0; i < teachers.size(); i++) {
            if (teachers.get(i).isCurator() == true) {
                System.out.println(teachers.get(i));
            }


        }
    }

    private static void males(ArrayList<Student> students, ArrayList<Teacher> teachers) {
        for (int i = 0; i < teachers.size() || i < students.size(); i++) {
            if (Gender.MALE == students.get(i).getGender() && Gender.MALE == teachers.get(i).getGender()) {
                System.out.println(students.get(i));
                System.out.println(teachers.get(i));
            }
        }
    }

}
