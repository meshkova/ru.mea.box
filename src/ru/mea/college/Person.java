package ru.mea.college;

public abstract class Person {
    protected Gender gender;
    protected String surname;

    public Person(Gender gender, String surname) {
        this.gender = gender;
        this.surname = surname;
    }

    public Gender getGender() {
        return gender;
    }

    public String getSurname() {
        return surname;
    }


}
