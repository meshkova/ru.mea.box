package ru.mea.college;


public class Student extends Person{

    private int year;
    private String code;

    public Student(Gender gender, String surname, int year, String code) {
        super(gender, surname);
        this.year = year;
        this.code = code;
    }

    public int getYear() {
        return year;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "Student{" + "пол=" + gender + ", фамилия=" + surname +
                ", year=" + year +
                ", code='" + code + '\'' +
                '}';
    }
}
