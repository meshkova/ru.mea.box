package ru.mea.box;

public class HeavyBox extends ColorBox {

    private double weight;

    public HeavyBox(String name, int width, int height, int depth, Color color, double v) {
        super(name, width, height, depth, color);
        this.weight = weight;
    }


    public double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return getName() + "{" +
                "высота=" + getHeight() +
                ", ширина=" + getWidth() +
                ", глубина=" + getDepth() +
                ", цвет=" + getColor() +
                ", вес=" + weight +
                '}';
    }
}