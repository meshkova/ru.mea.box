package ru.mea.box;



public class Main {

    public static void main(String[] args) {
        Box launchBox = new Box("ланчбокс ", 10, 4, 20);
        Box safe = new Box("сэйв ", 2, 5, 5);
        Box cubeBox = new Box("куб ", 5);
        Box standardBox = new Box();
        ColorBox colorBox = new ColorBox("Цветная", 2, 3, 4, Color.YELLOW);
        HeavyBox heavyBox = new HeavyBox("Подарок", 3, 4, 5,
                Color.GREEN, 2.7);
        System.out.println("Первая цветная " + colorBox);
        System.out.println("Тяжелая " + heavyBox);
        Box[] boxes =
                {launchBox, safe, standardBox,
                        cubeBox, colorBox, heavyBox};

        outArrayOfBox(boxes);


        System.out.println(safe.getWidth());
        safe.setWidth(3);
        System.out.println(safe.getWidth());
        System.out.println(cubeBox);
        System.out.println(standardBox);
        int v = safe.volume();
        System.out.println("Объём" + v + "куб.м.");
        int v2 = standardBox.volume();
        System.out.println(v2);
        int v3 = cubeBox.volume();
        System.out.println(v3);
        int v4 = launchBox.volume();
        System.out.println(v4);
        Box[] cubes = {safe, cubeBox, standardBox, launchBox};
        for (int i = 0; i < cubes.length; i++) {
            System.out.println((i + 1) + "кубик " + cubes[i]);

        }
        int[] a = {v, v2, v3, v4};
        int max = 0;
        for (int i = 0; i < a.length; i++) {
            if (max < a[i]) {
                max = a[i];
            }

        }
        if (max == v) {
            System.out.println(safe.getName() + " = " + max);
        }
        if (max == v2) {
            System.out.println(standardBox.getName() + " = " + max);
        }
        if (max == v3) {
            System.out.println(cubeBox.getName() + " = " + max);
        }
        if (max == v4) {
            System.out.println(launchBox.getName() + " = " + max);

        }

    }

    private static void outArrayOfBox (Box[]boxes){
        for (int i = 0; i < boxes.length; i++) {
            System.out.println((i + 1) + " коробочка " + boxes[i]);

        }
    }
}









