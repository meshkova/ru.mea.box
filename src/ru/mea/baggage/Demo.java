package ru.mea.baggage;



public class Demo {
    public static void main(String[] args) {
        Baggage ivanov = new Baggage("Иванов", 2, 45.67);
        Baggage petrova = new Baggage("Петрова", 2, 45.67);
        Baggage mishina = new Baggage("Мишина", 1, 3.5);
        Baggage[] people = {ivanov, petrova, mishina};

        System.out.print("Фамилии людей с багажом прошедшим условие: " );
        Sur(people);


    }


    /**
     * Sur вычисляет сколько человек удовлетворяет данному условию
     * и выводит фамилию на экран
     * @param people
     */
    public static void Sur(Baggage[] people) {

        for (int i = 0; i < people.length; i++) {
            if (people[i].bag() == true) {
                System.out.println(people[i].getSurname());
            }
        }

    }
}
