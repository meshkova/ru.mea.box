package ru.mea.baggage;

public class Baggage {
    private String surname;
    private int place;
    private double baggages;

    public Baggage(String surname, int place, double baggages) {
        this.surname = surname;
        this.place = place;
        this.baggages = baggages;
    }

    public Baggage() {
        this("не указана", 0, 0);
    }

    public String getSurname() {
        return surname;
    }

    public int getPlace() {
        return place;
    }

    public double getBaggages() {
        return baggages;
    }

    @Override
    public String toString() {
        return "Baggage{" +
                "surname=' " + surname + '\'' +
                ", place= " + place +
                ", baggages= " + baggages +
                '}';
    }


    /**
     * bag сравнивает поля по данному условию
     * @return
     */
    public boolean bag() {


        if (place == 1 && baggages < 10) {

            return true;
        } else {
            return false;
        }
    }
}
