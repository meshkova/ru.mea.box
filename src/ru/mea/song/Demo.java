package ru.mea.song;

import java.util.Scanner;


public class Demo {
    private static Scanner scanner = new Scanner(System.in);

    /**
     * Создаю список из 5 песен и вызываю метод isSameCategory
     * чтобы узнать одинаковые ли категории у первой и последней песни
     *
     * @param args имя
     */
    public static void main(String[] args) {
        Song love = new Song("Любовь...", "Егор Крид", 2);
        Song andrey = new Song("Андрей", "Алла Пугачёва", 1);
        Song summer = new Song("Лето", "Николай Басков", 0);
        Song whiteRose = new Song("Белые розы", "Юрий Шатунов", 6);
        Song autumn = new Song("Осень", "Кристина Арбакайте", 3);
        Song[] box = {love, andrey, summer, whiteRose, autumn};

        Songs(box);

        for (int i = 0; i < box.length; i++) {
            if (box[0].isSameCategory(box[box.length - 1])) {
                System.out.println("Первая и последня песни - схожи");
                break;
            } else {
                System.out.println("Несхожи");
            }
        }
    }

    /**
     * Сравниваю каждый объект из массива с разными категориями
     * Если объект подходит, то вывожу его
     *
     * @param box имя массива
     */
    public static void Songs(Song box[]) {
        int count = 0;
        System.out.println("Введите продолжительность песни");
        double c = scanner.nextDouble();

        if (c < 60 && c > 0) {
            for (Song song : box) {
                if (c == 1) {
                    if (song.category().equals("short")) {

                        System.out.println(song + "" + "short");
                        count++;

                    }
                } else if (c >= 2 && c <= 4) {
                    if (song.category().equals("medium")) {
                        System.out.println(song + "" + "medium");
                        count++;

                    }
                } else if (c > 4) {
                    if (song.category().equals("long")) {
                        System.out.println(song + "" + "long");
                        count++;

                    }
                }

            }


        } else if ((c % 1) > 0) {
            c = c / 1;
            for (Song song : box) {
                if (c == 1) {
                    if (song.category().equals("short")) {

                        System.out.println(song + "" + "short");
                        count++;

                    }
                } else if (c >= 2 && c <= 4) {
                    if (song.category().equals("medium")) {
                        System.out.println(song + "" + "medium");
                        count++;

                    }
                } else if (c > 4) {
                    if (song.category().equals("long")) {
                        System.out.println(song + "" + "long");
                        count++;

                    }
                }

            }
        } else if (count == 0) {
            System.out.println("Введите корректные числа");
        }
    }
}

