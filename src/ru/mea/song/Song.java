package ru.mea.song;



public class Song {
    private String name;
    private String performer;
    private int time;
    private int category;

    /**
     * конструктор с параметрами.
     * @param name название
     * @param performer автор
     * @param time продолжительность
     */
    public Song(String name, String performer, int time) {
        this.name = name;
        this.performer = performer;
        this.time = time;
    }

    /**
     * Конструктор по умолчанию.
     */

    public Song() {
        this("не указана", "не указан", 0);

    }

    /**
     * Геттеры это метод нужный, чтобы получить значения поля.
     * @return Возвращает строку
     */
    public String getName() {
        return name;
    }

    public String getPerformer() {
        return performer;
    }

    public int getTime() {
        return time;
    }

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", performer='" + performer + '\'' +
                ", time=" + time +
                '}';
    }

    /**
     * Метод сравнивает геттер с критерием и возвращает значение категории
     * Категории к которой относится геттер
     * @return возвращает категорию
     */
    public String category() {

        String category = "";
        if (getTime() == 1 ){
            category = "short";
            return category;
        }
        if (getTime() >= 2 && getTime() < 4) {
            category = "medium";
            return category;
        }
        if (getTime() > 4) {
            category = "long";
            return category;
        }
        return category;
    }
    /**Возвращает булевое значение исходя от того, равны категории(одинаковы) или нет
     *
     * @param love название первой песни
     * @return возвращает истину или ложь
     */
    boolean isSameCategory(Song love) {
        return (this.category()).equals(love.category());
    }
}

