package ru.mea.rational;


public class Rational {

    private int num;
    private int denom;

    public Rational(int num, int denom) {
        this.num = num;
        this.denom = denom;
    }

    public Rational(String b) {
        this(0, 0);
    }


    @Override
    public String toString() {
        return num + "/" + denom;
    }


    public int getNum() {
        return num;
    }

    public int getDenom() {
        return denom;
    }


    private static int rrr(Rational a) {
        int nod = 0;

        for (int i = 2; i <= a.getNum(); i++) {
            if (a.getNum() % i == 0 && a.getDenom() % i == 0)
                nod = i;

        }
        return nod;

    }

    private static int rrr2(Rational b) {
        int nod = 0;

        for (int i = 2; i <= b.getNum(); i++) {
            if (b.getNum() % i == 0 && b.getDenom() % i == 0)
                nod = i;

        }
        return nod;

    }

    static String add(Rational a, Rational b) {
        int nod = 0;
        int division;
        int nok1 = 0;
        int x2 = 0;
        int division2;


        int chislitel1 = a.getNum() / rrr(a);
        int chislitel2 = b.getNum() / rrr2(b);
        int znamenatel = a.getDenom() / rrr(a);
        int znamenatel2 = b.getDenom() / rrr2(b);

        for (int i = 2; i <= znamenatel; i++) {
            if (znamenatel % i == 0 && znamenatel2 % i == 0) {
                nod = i;
            }
            nok1 = (znamenatel * znamenatel2) / nod;
            division = nok1 / znamenatel; //Дополнительный множитель к числителю
            division2 = nok1 / znamenatel2;
            x2 = (chislitel1 * division) + (chislitel2 * division2);
        }
        return x2 + "/" + nok1;
    }

    static String sub(Rational a, Rational b) {
        int nod = 0;
        int division;
        int nok1 = 0;
        int drob = 0;
        int division2;
        int chislitel1 = a.getNum() / rrr(a);
        int chislitel2 = b.getNum() / rrr2(b);
        int znamenatel = a.getDenom() / rrr(a);
        int znamenatel2 = b.getDenom() / rrr2(b);


        for (int i = 2; i <= znamenatel; i++) {
            if (znamenatel % i == 0 && znamenatel2 % i == 0) {
                nod = i;
            }
            nok1 = (znamenatel * znamenatel2) / nod;
            division = nok1 / znamenatel; //Дополнительный множитель к числителю
            division2 = nok1 / znamenatel2;
            drob = (chislitel1 * division) - (chislitel2 * division2);
        }
        return drob + "/" + nok1;
    }

    static String mul(Rational a, Rational b) {
        int nod = 0;
        int chislitel1 = a.getNum() / rrr(a);
        int chislitel2 = b.getNum() / rrr2(b);
        int znamenatel = a.getDenom() / rrr(a);
        int znamenatel2 = b.getDenom() / rrr2(b);
        int mul2 = chislitel1 * chislitel2;
        int div2 = znamenatel * znamenatel2;
        for (int i = 2; i <= mul2; i++) {
            if (mul2 % i == 0 && div2 % i == 0) {
                nod = i;
            }
            mul2 = mul2 / nod;
            div2 = div2 / nod;
        }
        return mul2 + "/" + div2;
    }
    static String division(Rational a, Rational b) {
        int nod = 0;
        int chislitel1 = a.getNum() / rrr(a);
        int chislitel2 = b.getNum() / rrr2(b);
        int znamenatel = a.getDenom() / rrr(a);
        int znamenatel2 = b.getDenom() / rrr2(b);
        int mul2 = chislitel1 * znamenatel2;
        int div2 = chislitel2 * znamenatel;
        for (int i = 2; i <= mul2; i++) {
            if (mul2 % i == 0 && div2 % i == 0) {
                nod = i;
            }
            mul2 = mul2 / nod;
            div2 = div2 / nod;

        }
        return mul2 + "/" + div2;
    }
}






