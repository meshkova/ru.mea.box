package ru.mea.rational;


import java.util.Scanner;




public class Demo {
    private static Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {

        Rational a = input();


        Rational b = input();


        System.out.println("Сложение: " + Rational.add(a, b));
        System.out.println("Вычитание: " + Rational.sub(a, b));
        System.out.println("Умножение: " + Rational.mul(a, b));
        System.out.println("Деление: " + Rational.division(a, b));

    }

    // Ввод второй дроби
    private static Rational input() {

        System.out.println("Введите числитель");
        int num = scanner.nextInt();

        System.out.println("Введите знаменатель");
        int denom = scanner.nextInt();


        return new Rational(num, denom);

    }
}
