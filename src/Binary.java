import java.util.Scanner;

public class Binary {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        binar(-386);
        binar2(2.5);
        binar4();
    }
    /**Метод java.lang.Integer.toBinaryString ()
     * возвращает строковое представление целочисленного аргумента в виде целого числа без знака в базе 2.
     * Он принимает аргумент в типе данных Int и возвращает соответствующую двоичную строку.
     *
     *
     * @param i параметр
     */
    private static void binar(int i) {
        String inBits = Integer.toBinaryString(i);
        System.out.println("Разряды числа: " + inBits);
    }

    /**Метод java.lang.Double.doubleToLongBits ()
     * класса Java Double - это встроенная функция в java,
     * которая возвращает представление указанного значения с плавающей запятой
     * в соответствии с битовой компоновкой IEEE 754 с двойным форматом с плавающей запятой.
     *Принимает только один параметр
     */
    private static void binar2(double i) {

        String sResult = "";
        long numberBits = Double.doubleToLongBits(i);

        sResult = Long.toBinaryString(numberBits);
        System.out.println("Представление вещественного числа в формате чисел с плавающей точкой");
        System.out.format("Число: %5.2f\n", i);
        System.out.println("Формат чисел с плавающей точкой:");
        System.out.println(i > 0 ? "0" + sResult : sResult);
    }
    /**
     * Вытеснение нулей
     * На представление мантиссы предоставлено всего 52 бита
     */
    private static void binar4(){
        double d = 5.0/7;
        System.out.format("Число: %10.16f\n", d);
        d  +=300000;
        System.out.format("Число: %10.16f\n", d);
    }
}

